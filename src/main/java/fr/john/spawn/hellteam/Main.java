package fr.john.spawn.hellteam;

import fr.john.spawn.hellteam.bean.AlbumBean;
import fr.john.spawn.hellteam.bean.ArtisteBean;
import fr.john.spawn.hellteam.bean.ChansonBean;
import fr.john.spawn.hellteam.bean.CsvBean;
import fr.john.spawn.hellteam.error.FileException;
import fr.john.spawn.hellteam.model.*;
import fr.john.spawn.hellteam.util.file.UtilFile;

import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Classe de lancement du programme
 */
public class Main {

    /**
     * Programme principal
     *
     * @param args Arguments non utilisés
     */
    public static void main(String[] args) {
        new Main();
    }

    /**
     * Programe principal non static
     */
    private Main() {

        try {
            System.out.println("Lancement du projet");

            ModelArtiste modelArtiste = new ModelArtiste();
            ModelAlbum modelAlbum = new ModelAlbum();
            ModelChanson modelChanson = new ModelChanson();
            ModelChansonArtiste modelChansonArtiste = new ModelChansonArtiste();

            Properties fileProperties = UtilFile.getFileProperty("file.properties");
            List<CsvBean> listBeans = ModelFile.getListBeansCsv(
                    fileProperties.getProperty("file.path")
                            + fileProperties.getProperty("file.csv.libelle"));

            Map<String, ArtisteBean> mapArtistes = modelArtiste.updateArtistes(listBeans);
            Map<String, AlbumBean> mapAlbums = modelAlbum.updateAlbums(listBeans);
            Map<String, ChansonBean> mapChansons = modelChanson.updateChansons(listBeans, mapAlbums);

            modelChansonArtiste.update(mapArtistes, mapChansons, listBeans);

            modelChansonArtiste.recuperationAllEnCours();

            UtilFile.createFileRapport();

        } catch (Exception ex) {
            try {
                UtilFile.getStrategyFile().addErreur(ex);
                UtilFile.createFileRapport();
            } catch (FileException e) {
                System.err.println(e);
            }
        }


    }

}

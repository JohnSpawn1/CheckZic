package fr.john.spawn.hellteam.util.file;

import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Classe abstraite sur les écritures dans les fichiers
 */
abstract class AStrategyFile {

    List<String> lines;

    /**
     * Constructeur
     */
    AStrategyFile(){
        lines = new ArrayList<>();
        LocalDate now = LocalDate.now();
        String espace = " ";

        lines.add(
                "RAPPORT DU "
                + now.getDayOfMonth()
                + espace
                + now.getMonth().getDisplayName(TextStyle.FULL, Locale.FRENCH)
                + espace
                + now.getYear()
        );
    }

    List<String> getLines(){
        return lines;
    }

}

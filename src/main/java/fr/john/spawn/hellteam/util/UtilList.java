package fr.john.spawn.hellteam.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Classe utilitaire sur les listes
 */
public class UtilList {

    private UtilList() {
    }

    /**
     * Suppression de doublons dans une liste
     *
     * @param list List avec doublons
     * @return List sans doublons
     */
    public static List<String> deleteDoublons(List<String> list) {
        return new ArrayList<>(new HashSet<>(list));
    }

}

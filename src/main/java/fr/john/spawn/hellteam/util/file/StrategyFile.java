package fr.john.spawn.hellteam.util.file;

import fr.john.spawn.hellteam.bean.ChansonArtisteBean;
import fr.john.spawn.hellteam.bean.dto.AlbumDTO;
import fr.john.spawn.hellteam.bean.dto.ArtisteDTO;
import fr.john.spawn.hellteam.bean.dto.ChansonDTO;
import org.apache.commons.text.WordUtils;

import java.util.List;

/**
 * Classe gérant la strategy sur l'écriture du rapport
 */
public class StrategyFile extends AStrategyFile {

    private static final String TAB_2 = "\t\t";

    /**
     * Ajout des nouveaux artistes
     *
     * @param listArtistes Liste des nouveaux artistes
     */
    public void addArtistes(List<ArtisteDTO> listArtistes) {
        lines.add("\n\n\t- LISTE DES ARTISTES: ");

        lines.add("");

        if (listArtistes.isEmpty()) {
            lines.add(TAB_2 + " Aucun nouvel artiste n'a été ajouté");
        } else {
            listArtistes.forEach(artiste -> lines.add(TAB_2 + " - " + artiste.getLibelle().toUpperCase()));
        }

    }

    /**
     * Ajout des nouveaux albums
     *
     * @param listAlbums Liste des nouveaux albums
     */
    public void addAlbums(List<AlbumDTO> listAlbums) {

        lines.add("\n\n\t- LISTE DES ALBUMS: ");
        lines.add("");

        if (listAlbums.isEmpty()) {
            lines.add(TAB_2 + " Aucun nouvel album n'a été ajouté");
        } else {
            listAlbums.forEach(album -> lines.add(TAB_2 + " - " + WordUtils.capitalize(album.getLibelle())));
        }

    }

    /**
     * Ajout des nouvelles chansons
     *
     * @param listChansons Liste des nouvelles chansons
     */
    public void addChansons(List<ChansonDTO> listChansons) {

        lines.add("\n\n\t- LISTE DES CHANSONS: ");
        lines.add("");

        if (listChansons.isEmpty()) {
            lines.add(TAB_2 + " Aucune nouvelle chanson n'a été ajoutée");
        } else {
            listChansons.forEach(chanson -> lines.add(TAB_2 + " - " + WordUtils.capitalize(chanson.getLibelle())));
        }

    }

    public void addEnCours(List<ChansonArtisteBean> allEnCours) {

        lines.add("\n\n\t- LISTE DES CHANSONS COMPLETES EN COURS: ");
        lines.add("");

        allEnCours.forEach(tca -> lines.add(TAB_2 + " - "
                + WordUtils.capitalize(tca.getChansonBean().getLibelle()
                + " - "
                + tca.getArtisteBean().getLibelle().toUpperCase()
                + " - "
                + WordUtils.capitalize(tca.getChansonBean().getAlbumBean().getLibelle()))));

    }

    /**
     * Ajout de l'erreur rencontrée dans le fichier de rapport
     *
     * @param ex Message d'erreur
     */
    public void addErreur(Exception ex) {
        lines.add("ERREUR" + ex);

    }
}

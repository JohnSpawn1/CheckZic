package fr.john.spawn.hellteam.util.file;

import fr.john.spawn.hellteam.error.FileException;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Properties;

/**
 * Classe utilitaire sur la gestion des fichiers
 */
public class UtilFile {

    private static StrategyFile strategyFile = new StrategyFile();

    /**
     * Constructeur (not used)
     */
    private UtilFile() {
    }

    /**
     * Récupération d'un fichier properties
     *
     * @param filename Nom du fichier
     * @return Fichier properties renseigné
     * @throws FileException Erreur lors de la récupération du fichier
     */
    public static Properties getFileProperty(String filename) throws FileException {
        InputStream resourceAsStream = UtilFile.class.getClassLoader().getResourceAsStream(filename);
        Properties fileProperties = new Properties();
        if (null == resourceAsStream) {
            throw new FileException(filename, null);
        }
        try {
            fileProperties.load(resourceAsStream);
        } catch (IOException ex) {
            throw new FileException(filename, ex.getMessage());
        }
        return fileProperties;
    }

    /**
     * Création du fichier de rapport
     *
     * @throws FileException Erreur d'écriture du fichier
     */
    public static void createFileRapport() throws FileException {

        try {
            Properties fileProperties = UtilFile.getFileProperty("file.properties");
            LocalDate now = LocalDate.now();
            String zero = "0";

            String month = (now.getMonthValue() < 10) ? zero + now.getMonthValue() : String.valueOf(now.getMonthValue());
            String day = (now.getDayOfMonth() < 10) ? zero + now.getDayOfMonth() : String.valueOf(now.getDayOfMonth());

            Path file = Paths.get(
                    fileProperties.getProperty("file.path") +
                            fileProperties.getProperty("file.rapport.libelle")
                            + now.getYear() + month + day
                            + ".txt");

            Files.write(file, strategyFile.getLines(), StandardCharsets.UTF_8);

        } catch (FileException | IOException ex) {
            throw new FileException("Fichier de rapport", ex.getMessage());
        }


    }

    public static StrategyFile getStrategyFile() {
        return strategyFile;
    }
}

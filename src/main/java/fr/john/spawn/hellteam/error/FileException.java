package fr.john.spawn.hellteam.error;

/**
 * Classe gérant les erreurs de fichier
 */
public class FileException extends Exception {

    /**
     * Erreur la récupération d'un fichier
     *
     * @param file    Fichier ou l'erreur s'est produit
     * @param message Message d'erreur
     */
    public FileException(String file, String message) {
        super("Erreur sur la récupération du fichier: " + file + " : \n " + message);
    }

}

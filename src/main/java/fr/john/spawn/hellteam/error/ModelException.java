package fr.john.spawn.hellteam.error;

/**
 * Classe gérant les erreurs sur la partie 'Modèle'
 */
public class ModelException extends Exception {

    /**
     * Erreur sur la partie Modèle
     *
     * @param message Message d'erreur
     */
    public ModelException(String message){
        super(message);
    }

}

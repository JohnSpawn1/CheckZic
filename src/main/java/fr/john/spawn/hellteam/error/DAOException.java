package fr.john.spawn.hellteam.error;

/**
 * Classe gérant les erreurs sur la base de données
 */
public class DAOException extends Exception {

    /**
     * Erreur de connecion à la base de données
     *
     * @param message Message d'erreur
     */
    public DAOException(String message) {
        super("Erreur de connection à la base de données : \n" + message);
    }

    /**
     * Erreur de requête
     *
     * @param table   Table de la base de données ou s'est produit l'erreur
     * @param requete Requête ou s'est produit l'erreur
     * @param message Message d'erreur
     */
    public DAOException(String table, String requete, String message) {
        super("Erreur de requête " + requete + " sur la table " + table + " : \n" + message);
    }

}

package fr.john.spawn.hellteam.error;

/**
 * Classe gérant les erreurs de mapping
 */
public class MappingException extends Exception {

    /**
     * Erreur de mapping
     *
     * @param beanDepart  Bean à mapper
     * @param beanArrivee Bean mappé
     */
    public MappingException(String beanDepart, String beanArrivee) {
        super("Erreur de mapping sur " + beanDepart + " -> " + beanArrivee);
    }

}

package fr.john.spawn.hellteam.dao.strategy;

/**
 * Classe abstraite gérant les clauses WHERE des DAOs
 */
public abstract class AStrategyDAO {

    String strategy = "";

    /**
     * Récupération de la clause WHERE
     *
     * @return Clause WHERE renseignée
     */
    public String getStrategy() {
        return strategy;
    }

}

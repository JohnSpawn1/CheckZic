package fr.john.spawn.hellteam.dao;

import fr.john.spawn.hellteam.bean.dto.ArtisteDTO;
import fr.john.spawn.hellteam.dao.strategy.AStrategyDAO;
import fr.john.spawn.hellteam.error.DAOException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe gérant les requêtes sur la table ARTISTE
 */
public class ArtisteDAO extends ADAO<ArtisteDTO> {

    private static final String TABLE = "T_ARTISTE_ART";
    private static final String COLUMNS = "ART_LIBELLE";
    private static final String ID = "ART_ID";

    /**
     * Constructeur
     */
    public ArtisteDAO() {
        super();
    }

    @Override
    public void create(List<ArtisteDTO> list) throws DAOException {

        String sql = "INSERT INTO " + TABLE + "(" + COLUMNS + ") VALUES (?)";
        String requete = "CREATE";

        String[] messageErreur = new String[1];

        try (PreparedStatement statement = getConnection().prepareStatement(sql)) {

            if (!list.isEmpty()) {
                list.forEach(artiste -> {
                    try {
                        statement.setString(1, artiste.getLibelle());
                        statement.executeUpdate();
                    } catch (SQLException ex) {
                        messageErreur[0] = ex.getMessage();
                    }
                });

                if (messageErreur[0] != null && !messageErreur[0].isEmpty()) {
                    throw new DAOException(TABLE, requete, messageErreur[0]);
                }
            }

        } catch (SQLException ex) {
            throw new DAOException(TABLE, requete, ex.getMessage());
        }
    }

    @Override
    public List<ArtisteDTO> read(AStrategyDAO strategy) throws DAOException {

        String sql = "SELECT " + ID + ", " + COLUMNS + " FROM " + TABLE + strategy.getStrategy();

        try (PreparedStatement statement = getConnection().prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {

            List<ArtisteDTO> artistesExistants = new ArrayList<>();
            while (resultSet.next()) {
                ArtisteDTO artisteDTO = new ArtisteDTO();
                artisteDTO.setId(resultSet.getInt(1));
                artisteDTO.setLibelle(resultSet.getString(2));
                artistesExistants.add(artisteDTO);
            }
            return artistesExistants;

        } catch (SQLException ex) {
            throw new DAOException(TABLE, "READ", ex.getMessage());
        }
    }

    @Override
    public void update(List<ArtisteDTO> objects) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ArtisteDTO get(Long id) throws DAOException {
        return null;
    }
}

package fr.john.spawn.hellteam.dao;

import fr.john.spawn.hellteam.bean.dto.AlbumDTO;
import fr.john.spawn.hellteam.dao.strategy.AStrategyDAO;
import fr.john.spawn.hellteam.error.DAOException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AlbumDAO extends ADAO<AlbumDTO> {

    private static final String TABLE = "T_ALBUM_ALB";
    private static final String COLUMNS = "ALB_LIBELLE";
    private static final String ID = "ALB_ID";

    @Override
    public void create(List<AlbumDTO> list) throws DAOException {

        String sql = "INSERT INTO " + TABLE + "(" + COLUMNS + ") VALUES (?)";
        String[] messageErreur = new String[1];
        String requete = "create";

        try (PreparedStatement statement = getConnection().prepareStatement(sql)) {
            list.forEach(album -> {
                try {
                    statement.setString(1, album.getLibelle().toLowerCase());
                    statement.executeUpdate();
                } catch (SQLException ex) {
                    messageErreur[0] = ex.getMessage() + "\n";
                }
            });

            if (null != messageErreur[0] && !messageErreur[0].isEmpty()) {
                throw new DAOException(TABLE, requete, messageErreur[0]);
            }
        } catch (SQLException ex) {
            throw new DAOException(TABLE, requete, ex.getMessage());
        }
    }

    @Override
    public List<AlbumDTO> read(AStrategyDAO strategy) throws DAOException {

        String sql = "SELECT " + ID + ", " + COLUMNS + " FROM " + TABLE + strategy.getStrategy();

        try (PreparedStatement statement = getConnection().prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {

            List<AlbumDTO> listAlbums = new ArrayList<>();
            while (resultSet.next()) {
                AlbumDTO albumDTO = new AlbumDTO();
                albumDTO.setId(resultSet.getInt(1));
                albumDTO.setLibelle(resultSet.getString(2));
                listAlbums.add(albumDTO);
            }
            return listAlbums;
        } catch (SQLException ex) {
            throw new DAOException(TABLE, "read", ex.getMessage());
        }
    }

    @Override
    public void update(List<AlbumDTO> objects) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public AlbumDTO get(Long id) throws DAOException {
        String sql = " SELECT " + ID + ", " + COLUMNS + " FROM " + TABLE + " WHERE " + ID + " = ?";

        try (PreparedStatement statement = getConnection().prepareStatement(sql)) {

            statement.setLong(1, id);

            AlbumDTO albumDTO = new AlbumDTO();
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    albumDTO.setId(resultSet.getLong(1));
                    albumDTO.setLibelle(resultSet.getString(2));
                }
            }
            return albumDTO;


        } catch (SQLException ex) {
            throw new DAOException(TABLE, "GET", ex.getMessage());
        }
    }
}

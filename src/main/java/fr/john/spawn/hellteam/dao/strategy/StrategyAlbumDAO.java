package fr.john.spawn.hellteam.dao.strategy;

import fr.john.spawn.hellteam.bean.dto.AlbumDTO;

import java.util.List;

/**
 * Classe gérant les WHERE de AlbumDAO
 */
public class StrategyAlbumDAO extends AStrategyDAO {

    /**
     * Ajout de la clause WHERE ALB_LIBELLE IN
     *
     * @param albums Liste des libelles des albums
     */
    public void addSqlWhereLibelleIn(List<String> albums) {
        int[] size = new int[1];
        strategy = " WHERE ALB_LIBELLE IN (";

        albums.forEach(album -> {
            strategy += "'" + album.toLowerCase() + "'";
            size[0]++;

            if (size[0] < albums.size()) {
                strategy += ", ";
            }
        });

        strategy += ")";

    }

    /**
     * Liste des albums ou récupérer le libellé
     *
     * @param listAllAlbums Liste des albums
     */
    public void addSqlWhereLibelleFromListAlbumsIn(List<AlbumDTO> listAllAlbums) {
        int[] size = new int[1];
        strategy = " WHERE ALB_LIBELLE IN (";

        listAllAlbums.forEach(album -> {
            strategy += "'" + album.getLibelle().toLowerCase() + "'";
            size[0]++;

            if (size[0] < listAllAlbums.size()) {
                strategy += ", ";
            }
        });
        strategy += ")";
    }
}

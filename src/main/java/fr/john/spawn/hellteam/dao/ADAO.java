package fr.john.spawn.hellteam.dao;

import fr.john.spawn.hellteam.dao.strategy.AStrategyDAO;
import fr.john.spawn.hellteam.error.DAOException;
import fr.john.spawn.hellteam.error.FileException;
import fr.john.spawn.hellteam.util.file.UtilFile;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

/**
 * Classe abstraite gérant les différentes requêtes
 *
 * @param <T> Bean DTO
 */
public abstract class ADAO<T> {

    private static Connection connection;

    /**
     * Récupération de la connexion à a base de données
     *
     * @return Connexion effective
     * @throws DAOException Erreur de connexion
     */
    static Connection getConnection() throws DAOException {
        if (null == connection) {
            try {
                Properties properties = UtilFile.getFileProperty("database.properties");
                connection = DriverManager.getConnection(properties.getProperty("url"), properties);
            } catch (SQLException | FileException ex) {
                throw new DAOException(ex.getMessage());
            }
        }
        return connection;
    }

    /**
     * Constructeur
     */
    ADAO() {
    }

    public abstract void create(List<T> list) throws DAOException;

    public abstract List<T> read(AStrategyDAO strategy) throws DAOException;

    public abstract void update(List<T> objects) throws DAOException;

    public abstract void delete(int id) throws DAOException;

    public abstract T get(Long id) throws DAOException;

}

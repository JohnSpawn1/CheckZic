package fr.john.spawn.hellteam.dao;

import fr.john.spawn.hellteam.bean.dto.ChansonDTO;
import fr.john.spawn.hellteam.dao.strategy.AStrategyDAO;
import fr.john.spawn.hellteam.error.DAOException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe gérant les requêtes sur la table CHANSON
 */
public class ChansonDAO extends ADAO<ChansonDTO> {

    private static final String TABLE = "T_CHANSON_CHA";
    private static final String ID = "cha_id";
    private static final String COLUMNS = "cha_alb, cha_libelle, cha_en_cours";

    @Override
    public void create(List<ChansonDTO> list) throws DAOException {

        String sql = "INSERT INTO " + TABLE + "(" + COLUMNS + ") VALUES (?, ?, ?)";
        String[] messageErreur = new String[1];
        String requete = "INSERT";

        try (PreparedStatement statement = getConnection().prepareStatement(sql)) {

            list.forEach(chanson -> {
                try {
                    statement.setLong(1, chanson.getIdAlbum());
                    statement.setString(2, chanson.getLibelle().toLowerCase());
                    statement.setBoolean(3, true);
                    statement.executeUpdate();
                } catch (SQLException ex) {
                    messageErreur[0] = ex.getMessage();
                }
            });

            if (null != messageErreur[0] && !messageErreur[0].isEmpty()) {
                throw new DAOException(TABLE, requete, messageErreur[0]);
            }

        } catch (SQLException ex) {
            throw new DAOException(TABLE, requete, ex.getMessage());
        }
    }

    @Override
    public List<ChansonDTO> read(AStrategyDAO strategy) throws DAOException {

        String sql = "SELECT " + ID + ", " + COLUMNS + " FROM " + TABLE + strategy.getStrategy();

        try (PreparedStatement statement = getConnection().prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {

            List<ChansonDTO> listChansons = new ArrayList<>();
            while (resultSet.next()) {
                ChansonDTO chansonDTO = new ChansonDTO();
                chansonDTO.setId(resultSet.getLong(1));
                chansonDTO.setIdAlbum(resultSet.getLong(2));
                chansonDTO.setLibelle(resultSet.getString(3));
                chansonDTO.setEnCours(resultSet.getBoolean(4));

                listChansons.add(chansonDTO);
            }

            return listChansons;

        } catch (SQLException ex) {
            throw new DAOException(TABLE, "READ", ex.getMessage());
        }

    }

    @Override
    public void update(List<ChansonDTO> objects) throws DAOException {

        String sql = "UPDATE T_CHANSON_CHA SET cha_en_cours = ? WHERE cha_alb = ? AND cha_libelle = ?";
        String[] messageErreur = new String[1];
        String requete = "UPDATE";


        try (PreparedStatement statement = getConnection().prepareStatement(sql)) {
            objects.forEach(object -> {
                try {
                    statement.setBoolean(1, object.isEnCours());
                    statement.setLong(2, object.getIdAlbum());
                    statement.setString(3, object.getLibelle());

                    statement.executeUpdate();
                } catch (SQLException ex) {
                    messageErreur[0] = ex.getMessage();
                }
            });

            if (null != messageErreur[0] && !messageErreur[0].isEmpty()) {
                throw new DAOException(TABLE, requete, messageErreur[0]);
            }

        } catch (SQLException | DAOException ex) {
            throw new DAOException(TABLE, requete, ex.getMessage());
        }

    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ChansonDTO get(Long id) throws DAOException {
        return null;
    }
}

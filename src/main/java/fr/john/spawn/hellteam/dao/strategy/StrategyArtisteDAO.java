package fr.john.spawn.hellteam.dao.strategy;

import fr.john.spawn.hellteam.bean.dto.ArtisteDTO;

import java.util.List;

/**
 * Classe gérant les WHERE de ArtisteDAO
 */
public class StrategyArtisteDAO extends AStrategyDAO {

    /**
     * Ajout de la condition 'WHERE ART_LIBELLE IN'
     *
     * @param artistes Liste des libelles
     */
    public void addSqlLibelleIn(List<String> artistes) {

        int[] size = new int[1];
        strategy = " WHERE ART_LIBELLE IN (";
        artistes.forEach(artiste -> {
            strategy += "'" + artiste.toLowerCase() + "'";
            size[0]++;
            if (size[0] < artistes.size()) {
                strategy += ", ";
            }
        });

        strategy += ")";

    }

    /**
     * Ajout de la condition 'WHERE ART_LIBELLE IN'
     *
     * @param listAllArtist Liste des artistes ou récupérer le libellé
     */
    public void addSqlWhereLibelleFromArtistIn(List<ArtisteDTO> listAllArtist) {

        int[] size = new int[1];
        strategy = " WHERE ART_LIBELLE IN (";

        listAllArtist.forEach(artist -> {
            strategy += "'" + artist.getLibelle().toLowerCase() + "'";
            size[0]++;

            if (size[0] < listAllArtist.size()) {
                strategy += ", ";
            }
        });

        strategy += ")";

    }
}

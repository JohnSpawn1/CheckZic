package fr.john.spawn.hellteam.dao.strategy;

import fr.john.spawn.hellteam.bean.dto.ChansonArtisteDTO;

import java.util.List;

/**
 * Classe gérant les WHERE de 'ChansonArtisteDAO'
 */
public class StrategyChansonArtisteDAO extends AStrategyDAO {

    /**
     * Ajout du "WHERE (tca_cha, tca_art) IN( .... )
     *
     * @param list Liste des tca à inscrire dans le where
     */
    public void addSqlWhereIdChansonIdArtisteIn(List<ChansonArtisteDTO> list) {

        int[] size = new int[1];
        strategy = " WHERE (tca_cha, tca_art) IN(";

        list.forEach(tca -> {
            strategy += "(";
            strategy += tca.getIdChanson();
            strategy += ", ";
            strategy += tca.getIdArtiste();
            strategy += ")";

            size[0]++;
            if (size[0] < list.size()) {
                strategy += ", ";
            }
        });
        strategy += ")";

    }
}

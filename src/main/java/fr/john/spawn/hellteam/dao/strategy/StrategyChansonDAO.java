package fr.john.spawn.hellteam.dao.strategy;

import fr.john.spawn.hellteam.bean.dto.ChansonDTO;

import java.util.List;

/**
 * Classe gérant les WHERE sur le DAO chanson
 */
public class StrategyChansonDAO extends AStrategyDAO {

    /**
     * Ajout du sql WHERE libelle AND id_album IN
     *
     * @param listChansons Liste des chansons ou mettre les paramètres
     */
    public void addSqlWhereLibelleIdalbumIn(List<ChansonDTO> listChansons) {

        int[] size = new int[1];
        strategy = " WHERE (cha_libelle, cha_alb) IN (";

        listChansons.forEach(chanson -> {
            strategy += "('";
            strategy += chanson.getLibelle().toLowerCase().replace("'", "''");
            strategy += "', ";
            strategy += chanson.getIdAlbum();
            strategy += ")";

            size[0]++;
            if (size[0] < listChansons.size()) {
                strategy += ", ";
            }
        });
        strategy += ")";

    }

    /**
     * Ajout du sql WHERE CHA_EN_COURS = true
     */
    public void addSqlWhereEnCours() {
        strategy = " WHERE cha_en_cours = true";
    }


}

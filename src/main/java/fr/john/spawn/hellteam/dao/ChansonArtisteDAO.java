package fr.john.spawn.hellteam.dao;

import fr.john.spawn.hellteam.bean.AlbumBean;
import fr.john.spawn.hellteam.bean.ArtisteBean;
import fr.john.spawn.hellteam.bean.ChansonArtisteBean;
import fr.john.spawn.hellteam.bean.ChansonBean;
import fr.john.spawn.hellteam.bean.dto.ChansonArtisteDTO;
import fr.john.spawn.hellteam.dao.strategy.AStrategyDAO;
import fr.john.spawn.hellteam.error.DAOException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe gérant les requêtes sur la table 'TJ_CHANSON_ARTISTE_TCA'
 */
public class ChansonArtisteDAO extends ADAO<ChansonArtisteDTO> {

    private static final String TABLE = "TJ_CHANSON_ARTISTE_TCA";
    private static final String ID_CHA = "TCA_CHA";
    private static final String ID_ART = "TCA_ART";

    @Override
    public void create(List<ChansonArtisteDTO> list) throws DAOException {
        String sql = "INSERT INTO " + TABLE + " ( " + ID_CHA + ", " + ID_ART + ") VALUES (?, ?)";
        String requete = "CREATE";
        String[] message = new String[1];
        message[0] = "";
        try (PreparedStatement statement = getConnection().prepareStatement(sql)) {
            list.forEach(tca -> {
                try {
                    statement.setLong(1, tca.getIdChanson());
                    statement.setLong(2, tca.getIdArtiste());
                    statement.executeUpdate();
                } catch (SQLException ex) {
                    message[0] = ex.getMessage();
                }
            });
            if (!message[0].isEmpty()) {
                throw new DAOException(TABLE, requete, message[0]);
            }
        } catch (SQLException ex) {
            throw new DAOException(TABLE, requete, ex.getMessage());
        }
    }

    @Override
    public List<ChansonArtisteDTO> read(AStrategyDAO strategy) throws DAOException {
        String sql = "SELECT " + ID_CHA + ", " + ID_ART + " FROM " + TABLE + strategy.getStrategy();
        List<ChansonArtisteDTO> listTca = new ArrayList<>();
        try (PreparedStatement statement = getConnection().prepareStatement(sql);
             ResultSet result = statement.executeQuery()) {

            while (result.next()) {
                ChansonArtisteDTO tca = new ChansonArtisteDTO();
                tca.setIdChanson(result.getLong(1));
                tca.setIdArtiste(result.getLong(2));
                listTca.add(tca);
            }
            return listTca;
        } catch (SQLException ex) {
            throw new DAOException(TABLE, "READ", ex.getMessage());
        }
    }

    @Override
    public void update(List<ChansonArtisteDTO> objects) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ChansonArtisteDTO get(Long id) {
        throw new UnsupportedOperationException();
    }

    public List<ChansonArtisteBean> getAll() throws DAOException {

        List<ChansonArtisteBean> listEnCours = new ArrayList<>();
        String sql = "SELECT " +
                "TCA_CHA, TCA_ART, " +
                "ART_ID, ART_LIBELLE, " +
                "CHA_ID, CHA_ALB, CHA_LIBELLE, CHA_EN_COURS, " +
                "ALB_ID, ALB_LIBELLE " +
                "FROM TJ_CHANSON_ARTISTE_TCA " +
                "INNER JOIN T_ARTISTE_ART ON ART_ID = TCA_ART " +
                "INNER JOIN T_CHANSON_CHA ON TCA_CHA = CHA_ID " +
                "INNER JOIN T_ALBUM_ALB ON CHA_ALB = ALB_ID " +
                "WHERE CHA_EN_COURS = ?";

        try (PreparedStatement statement = getConnection().prepareStatement(sql)) {
            statement.setBoolean(1, true);

            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    AlbumBean album = new AlbumBean();
                    album.setId(resultSet.getLong(9));
                    album.setLibelle(resultSet.getString(10));

                    ChansonBean chanson = new ChansonBean();
                    chanson.setId(resultSet.getLong(5));
                    chanson.setAlbumBean(album);
                    chanson.setLibelle(resultSet.getString(7));
                    chanson.setEnCours(true);

                    ArtisteBean artiste = new ArtisteBean();
                    artiste.setId(resultSet.getLong(3));
                    artiste.setLibelle(resultSet.getString(4));

                    ChansonArtisteBean tca = new ChansonArtisteBean();
                    tca.setArtisteBean(artiste);
                    tca.setChansonBean(chanson);

                    listEnCours.add(tca);
                }
            }

            return listEnCours;

        } catch (SQLException | DAOException ex) {
            throw new DAOException(TABLE, "GETALL", ex.getMessage());
        }

    }
}

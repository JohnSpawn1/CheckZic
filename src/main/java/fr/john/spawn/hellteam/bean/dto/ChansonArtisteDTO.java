package fr.john.spawn.hellteam.bean.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChansonArtisteDTO {

    private Long idChanson;
    private Long idArtiste;

}

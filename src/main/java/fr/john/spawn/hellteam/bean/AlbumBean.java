package fr.john.spawn.hellteam.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AlbumBean {

    private Long id;
    private String libelle;

}

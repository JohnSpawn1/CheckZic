package fr.john.spawn.hellteam.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ArtisteBean {

    private Long id;
    private String libelle;

}

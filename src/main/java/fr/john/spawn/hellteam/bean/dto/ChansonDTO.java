package fr.john.spawn.hellteam.bean.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChansonDTO {

    private Long id;
    private Long idAlbum;
    private String libelle;
    private boolean enCours;

}

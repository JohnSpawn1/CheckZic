package fr.john.spawn.hellteam.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChansonBean {

    private Long id;
    private AlbumBean albumBean;
    private String libelle;
    private boolean enCours;

}

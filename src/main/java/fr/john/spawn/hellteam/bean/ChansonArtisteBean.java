package fr.john.spawn.hellteam.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChansonArtisteBean {

    private ArtisteBean artisteBean;
    private ChansonBean chansonBean;

}

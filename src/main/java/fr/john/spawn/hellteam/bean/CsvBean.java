package fr.john.spawn.hellteam.bean;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;


/**
 * Classe gérant le bean du fichier CSV
 */
@Getter
@Setter
public class CsvBean {

    private String title;
    private String artist;
    private String album;
    private int track;
    private int year;
    private long length;
    private String size;
    private LocalDate lastModified;
    private String path;
    private String filename;
    private String interpret;

}

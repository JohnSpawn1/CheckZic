package fr.john.spawn.hellteam.bean.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ArtisteDTO {

    private long id;
    private String libelle;

}

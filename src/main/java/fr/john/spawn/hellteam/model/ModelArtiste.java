package fr.john.spawn.hellteam.model;

import fr.john.spawn.hellteam.bean.ArtisteBean;
import fr.john.spawn.hellteam.bean.CsvBean;
import fr.john.spawn.hellteam.bean.dto.ArtisteDTO;
import fr.john.spawn.hellteam.dao.ArtisteDAO;
import fr.john.spawn.hellteam.dao.strategy.StrategyArtisteDAO;
import fr.john.spawn.hellteam.error.DAOException;
import fr.john.spawn.hellteam.error.ModelException;
import fr.john.spawn.hellteam.mapping.MappingArtiste;
import fr.john.spawn.hellteam.util.UtilList;
import fr.john.spawn.hellteam.util.file.UtilFile;

import java.util.*;

/**
 * Classe gérant les opérations sur la données 'Artiste'
 */
public class ModelArtiste {

    private ArtisteDAO artisteDao;

    /**
     * Constructeur
     *
     * @throws ModelException Erreur d'instanciation du DAO
     */
    public ModelArtiste() throws ModelException {
        try {
            artisteDao = new ArtisteDAO();
        } catch (Exception ex) {
            throw new ModelException(ex.getMessage());
        }
    }

    /**
     * Mise à jour de la liste des artistes
     *
     * @param listBeans Liste des beans à mettre à jour
     * @throws ModelException Erreur lors de la mise à jour
     */
    public Map<String, ArtisteBean> updateArtistes(List<CsvBean> listBeans) throws ModelException {

        try {
            List<String> artistes = new ArrayList<>();
            listBeans.forEach(bean -> {
                String[] multiplesArtistes = bean.getInterpret().toLowerCase().split(" feat. ");
                Collections.addAll(artistes, multiplesArtistes);
            });

            List<String> listNoDoublons = UtilList.deleteDoublons(artistes);

            StrategyArtisteDAO strategyArtiste = new StrategyArtisteDAO();
            strategyArtiste.addSqlLibelleIn(listNoDoublons);
            List<ArtisteDTO> artistesExistantsDto = artisteDao.read(strategyArtiste);

            boolean[] artisteTrouve = new boolean[1];

            List<String> listToCreate = new ArrayList<>();
            listNoDoublons.forEach(libelleNoDoublon -> {
                artistesExistantsDto.forEach(artisteDTO -> {
                    if (!artisteDTO.getLibelle().equals(libelleNoDoublon)) {
                        artisteTrouve[0] = true;
                    }
                });
                if (!artisteTrouve[0]) {
                    listToCreate.add(libelleNoDoublon);
                }
            });

            List<ArtisteDTO> listArtistDtoToCreate = new ArrayList<>();
            UtilList.deleteDoublons(listToCreate).forEach(obj -> {
                ArtisteDTO artisteDTO = new ArtisteDTO();
                artisteDTO.setLibelle(obj);
                listArtistDtoToCreate.add(artisteDTO);
            });

            artisteDao.create(listArtistDtoToCreate);

            List<ArtisteDTO> listAllArtist = new ArrayList<>();
            listAllArtist.addAll(listArtistDtoToCreate);
            listAllArtist.addAll(artistesExistantsDto);

            StrategyArtisteDAO strategy = new StrategyArtisteDAO();
            strategy.addSqlWhereLibelleFromArtistIn(listAllArtist);
            List<ArtisteDTO> artisteDtoCreated = artisteDao.read(strategy);

            UtilFile.getStrategyFile().addArtistes(listArtistDtoToCreate);

            List<ArtisteBean> listBean = new ArrayList<>();
            artisteDtoCreated.forEach(artisteDTO -> listBean.add(MappingArtiste.mappingArtistDtoToArtistBean(artisteDTO)));

            Map<String, ArtisteBean> mapArtist = new HashMap<>();
            listBean.forEach(bean -> mapArtist.put(bean.getLibelle(), bean));

            return mapArtist;

        } catch (DAOException ex) {
            throw new ModelException(ex.getMessage());
        }
    }
}

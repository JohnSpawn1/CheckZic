package fr.john.spawn.hellteam.model;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import fr.john.spawn.hellteam.bean.CsvBean;
import fr.john.spawn.hellteam.error.FileException;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe de gestion de fichiers
 */
public class ModelFile {

    /**
     * Constructeur (not used)
     */
    private ModelFile() {
    }

    /**
     * Récupère la liste
     *
     * @param path Chemin du fichier
     * @return Liste des différents objets
     */
    public static List<CsvBean> getListBeansCsv(String path) throws FileException {

        try (Reader reader = Files.newBufferedReader(Paths.get(path))) {

            CSVParser parser = new CSVParserBuilder()
                    .withSeparator(';')
                    .withIgnoreQuotations(true)
                    .build();
            CSVReader csvReader = new CSVReaderBuilder(reader)
                    .withSkipLines(1)
                    .withCSVParser(parser)
                    .build();

            List<String[]> records = csvReader.readAll();

            List<CsvBean> listCsv = new ArrayList<>();
            for (String[] record : records) {
                addBeans(listCsv, record);
            }

            return listCsv;

        } catch (IOException ex) {
            throw new FileException("fichier file", ex.getMessage());
        }

    }

    /**
     * Ajout des beans
     *
     * @param listCsv Liste ou ajouter le bean
     * @param record  Données du bean
     */
    private static void addBeans(List<CsvBean> listCsv, String[] record) {
        if (record.length > 2) {
            CsvBean bean = new CsvBean();
            bean.setTitle(record[0]);
            bean.setArtist(record[1]);
            bean.setAlbum(record[2]);
            if (StringUtils.isNotEmpty(record[3])) {
                bean.setTrack(Integer.parseInt(record[3]));
            }

            if (StringUtils.isNotEmpty(record[4])) {
                bean.setYear(Integer.parseInt(record[4]));
            }

            if (StringUtils.isNotEmpty(record[5])) {
                bean.setLength(Long.parseLong(record[5]));
            }
            bean.setSize(record[6]);
            if (record[7] != null) {
                String[] split = record[7].split("/");
                bean.setLastModified(LocalDate.of(
                        Integer.parseInt(split[2]),
                        Integer.parseInt(split[1]),
                        Integer.parseInt(split[0])));
            }
            bean.setPath(record[8]);
            bean.setFilename(record[9]);
            bean.setInterpret(record[10]);

            listCsv.add(bean);
        }
    }

}
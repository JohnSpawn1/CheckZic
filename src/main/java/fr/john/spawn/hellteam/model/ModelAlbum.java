package fr.john.spawn.hellteam.model;

import fr.john.spawn.hellteam.bean.AlbumBean;
import fr.john.spawn.hellteam.bean.CsvBean;
import fr.john.spawn.hellteam.bean.dto.AlbumDTO;
import fr.john.spawn.hellteam.dao.AlbumDAO;
import fr.john.spawn.hellteam.dao.strategy.StrategyAlbumDAO;
import fr.john.spawn.hellteam.error.DAOException;
import fr.john.spawn.hellteam.error.ModelException;
import fr.john.spawn.hellteam.mapping.MappingAlbum;
import fr.john.spawn.hellteam.util.UtilList;
import fr.john.spawn.hellteam.util.file.UtilFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Classe gérant les opérations sur les données 'Album'
 */
public class ModelAlbum {

    private AlbumDAO dao;
    private boolean albumExist;

    /**
     * Constructeur
     */
    public ModelAlbum() {
        dao = new AlbumDAO();
    }

    /**
     * Mise à jour de la liste des albums
     *
     * @param listBeans Liste des albums à mettre à jour
     */
    public Map<String, AlbumBean> updateAlbums(List<CsvBean> listBeans) throws ModelException {

        try {
            List<String> listAlbums = new ArrayList<>();
            listBeans.forEach(bean -> listAlbums.add(bean.getAlbum()));
            List<String> listAlbumsNoDoublons = UtilList.deleteDoublons(listAlbums);

            StrategyAlbumDAO strategyAlbumDAO = new StrategyAlbumDAO();
            strategyAlbumDAO.addSqlWhereLibelleIn(listAlbumsNoDoublons);
            List<AlbumDTO> albumsExistants = dao.read(strategyAlbumDAO);

            List<String> albumsToCreate = new ArrayList<>();
            listAlbumsNoDoublons.forEach(album -> {
                albumExist = false;
                albumsExistants.forEach(albumExistant -> {
                    if (!album.toLowerCase().equalsIgnoreCase(albumExistant.getLibelle().toLowerCase())) {
                        albumExist = true;
                    }
                });
                if (!albumExist) {
                    albumsToCreate.add(album.toLowerCase());
                }
            });

            List<AlbumDTO> listAlbumesToCreate = MappingAlbum.mappingLibellesAlbumToDto(albumsToCreate);
            dao.create(listAlbumesToCreate);

            List<AlbumDTO> listAllAlbums = new ArrayList<>();
            listAllAlbums.addAll(listAlbumesToCreate);
            listAllAlbums.addAll(albumsExistants);

            StrategyAlbumDAO strategy = new StrategyAlbumDAO();
            strategy.addSqlWhereLibelleFromListAlbumsIn(listAllAlbums);
            List<AlbumDTO> albumsDtoCreated = dao.read(strategy);

            UtilFile.getStrategyFile().addAlbums(listAlbumesToCreate);

            List<AlbumBean> listAlbumBeans = new ArrayList<>();
            albumsDtoCreated.forEach(albumDTO -> listAlbumBeans.add(MappingAlbum.mappingAlbumDtoToAlbumBean(albumDTO)));

            Map<String, AlbumBean> mapAlbums = new HashMap<>();
            listAlbumBeans.forEach(albumBean -> mapAlbums.put(albumBean.getLibelle(), albumBean));

            return mapAlbums;

        } catch (DAOException ex) {
            throw new ModelException(ex.getMessage());
        }

    }

}

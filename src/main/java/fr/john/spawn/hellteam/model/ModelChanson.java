package fr.john.spawn.hellteam.model;

import fr.john.spawn.hellteam.bean.AlbumBean;
import fr.john.spawn.hellteam.bean.ChansonBean;
import fr.john.spawn.hellteam.bean.CsvBean;
import fr.john.spawn.hellteam.bean.dto.ChansonDTO;
import fr.john.spawn.hellteam.dao.ChansonDAO;
import fr.john.spawn.hellteam.dao.strategy.StrategyChansonDAO;
import fr.john.spawn.hellteam.error.DAOException;
import fr.john.spawn.hellteam.error.MappingException;
import fr.john.spawn.hellteam.error.ModelException;
import fr.john.spawn.hellteam.mapping.MappingChanson;
import fr.john.spawn.hellteam.util.file.UtilFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Classe gérant les opérations sur 'Chanson'
 */
public class ModelChanson {

    private ChansonDAO dao;
    private MappingChanson mappingChanson;

    /**
     * Constructor (not used)
     */
    public ModelChanson() {
        dao = new ChansonDAO();
        mappingChanson = new MappingChanson();
    }

    /**
     * Mise à jour des chansons
     *
     * @param listBeans Liste des beans à mettre à jour
     * @param mapAlbums Map des albums existants
     * @throws ModelException Erreur lors de la mise à jour
     */
    public Map<String, ChansonBean> updateChansons(List<CsvBean> listBeans, Map<String, AlbumBean> mapAlbums)
            throws ModelException {

        try {

            StrategyChansonDAO strategy = new StrategyChansonDAO();
            strategy.addSqlWhereEnCours();
            List<ChansonDTO> listChansonsEnCours = dao.read(strategy);
            listChansonsEnCours.forEach(chanson -> chanson.setEnCours(false));
            dao.update(listChansonsEnCours);

            List<ChansonDTO> listChansons = new ArrayList<>();
            listBeans.forEach(bean -> {
                ChansonDTO chansonDTO = new ChansonDTO();
                chansonDTO.setLibelle(bean.getTitle());
                chansonDTO.setIdAlbum(mapAlbums.get(bean.getAlbum().toLowerCase()).getId());
                listChansons.add(chansonDTO);
            });

            StrategyChansonDAO strategyChansonDAO = new StrategyChansonDAO();
            strategyChansonDAO.addSqlWhereLibelleIdalbumIn(listChansons);
            List<ChansonDTO> oldChansons = dao.read(strategyChansonDAO);

            boolean[] chansonExist = new boolean[1];
            List<ChansonDTO> chansonToCreate = new ArrayList<>();
            List<ChansonDTO> chansonToUpdate = new ArrayList<>();
            listChansons.forEach(chanson -> {
                chansonExist[0] = false;
                oldChansons.forEach(oldChanson -> {
                    if (oldChanson.getLibelle().equalsIgnoreCase(chanson.getLibelle())
                            && oldChanson.getIdAlbum().equals(chanson.getIdAlbum())) {
                        chansonExist[0] = true;
                    }
                });

                if (!chansonExist[0]) {
                    chansonToCreate.add(chanson);
                } else {
                    chansonToUpdate.add(chanson);
                }
            });

            dao.create(chansonToCreate);
            chansonToUpdate.forEach(chanson -> {
                chanson.setEnCours(true);
                chanson.setLibelle(chanson.getLibelle().toLowerCase());
            });
            dao.update(chansonToUpdate);

            UtilFile.getStrategyFile().addChansons(chansonToCreate);

            List<ChansonDTO> allChansons = new ArrayList<>();
            allChansons.addAll(chansonToUpdate);
            allChansons.addAll(chansonToCreate);

            strategy = new StrategyChansonDAO();
            strategy.addSqlWhereLibelleIdalbumIn(allChansons);
            List<ChansonDTO> listChansonsLues = dao.read(strategy);


            List<ChansonBean> listChansonsLuesBean = new ArrayList<>();

            String[] messageErreur = new String[1];
            messageErreur[0] = "";
            listChansonsLues.forEach(chansonDTO -> {
                try {
                    listChansonsLuesBean.add(mappingChanson.mappingChansonDtoToChansonBean(chansonDTO));
                } catch (MappingException ex) {
                    messageErreur[0] = ex.getMessage();
                }
            });

            if (!messageErreur[0].isEmpty()) {
                throw new ModelException(messageErreur[0]);
            }

            Map<String, ChansonBean> mapChansons = new HashMap<>();
            listChansonsLuesBean.forEach(chanson -> mapChansons.put(chanson.getLibelle(), chanson));

            return mapChansons;

        } catch (DAOException ex) {
            throw new ModelException(ex.getMessage());
        }


    }
}

package fr.john.spawn.hellteam.model;

import fr.john.spawn.hellteam.bean.ArtisteBean;
import fr.john.spawn.hellteam.bean.ChansonArtisteBean;
import fr.john.spawn.hellteam.bean.ChansonBean;
import fr.john.spawn.hellteam.bean.CsvBean;
import fr.john.spawn.hellteam.bean.dto.ChansonArtisteDTO;
import fr.john.spawn.hellteam.dao.ChansonArtisteDAO;
import fr.john.spawn.hellteam.dao.strategy.StrategyChansonArtisteDAO;
import fr.john.spawn.hellteam.error.DAOException;
import fr.john.spawn.hellteam.error.ModelException;
import fr.john.spawn.hellteam.util.file.UtilFile;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Classe gérant les opérations du lien entre les artistes et les chansons
 */
public class ModelChansonArtiste {

    private ChansonArtisteDAO dao;

    /**
     * Constructeur
     */
    public ModelChansonArtiste() {
        dao = new ChansonArtisteDAO();
    }

    /**
     * Mise à jour du lien entre les artistes et les chansons
     *
     * @param mapArtistes Map contenant les artistes en cours
     * @param mapChansons Map contenant les chansons en cours
     * @param listBeans   Liste des beans en cours
     * @throws ModelException Erreur lors de la mise à jour
     */
    public void update(Map<String, ArtisteBean> mapArtistes, Map<String, ChansonBean> mapChansons, List<CsvBean> listBeans)
            throws ModelException {
        try {

            List<ChansonArtisteDTO> listTac = new ArrayList<>();

            listBeans.forEach(bean -> {

                String[] multiplesArtistes = bean.getInterpret().toLowerCase().split(" feat. ");
                List<String> listArtist = Arrays.asList(multiplesArtistes);

                listArtist.forEach(artist -> {
                    ChansonArtisteDTO artisteChansonDTO = new ChansonArtisteDTO();
                    artisteChansonDTO.setIdArtiste(mapArtistes.get(artist).getId());
                    artisteChansonDTO.setIdChanson(mapChansons.get(bean.getTitle().toLowerCase()).getId());
                    listTac.add(artisteChansonDTO);
                });
            });

            StrategyChansonArtisteDAO strategy = new StrategyChansonArtisteDAO();
            strategy.addSqlWhereIdChansonIdArtisteIn(listTac);
            List<ChansonArtisteDTO> listTcaExistant = dao.read(strategy);

            List<ChansonArtisteDTO> listTcaToCreate = new ArrayList<>();
            boolean[] tcaExist = new boolean[1];
            listTac.forEach(tca -> {
                tcaExist[0] = false;
                listTcaExistant.forEach(tcaExistant -> {
                    if (tca.getIdArtiste().longValue() == tcaExistant.getIdArtiste().longValue()
                            && tca.getIdChanson().longValue() == tcaExistant.getIdChanson().longValue()) {
                        tcaExist[0] = true;
                    }
                });
                if (!tcaExist[0]) {
                    listTcaToCreate.add(tca);
                }
            });

            dao.create(listTcaToCreate);
        } catch (DAOException ex) {
            throw new ModelException(ex.getMessage());
        }

    }

    /**
     * Récupération de toutes les chansons en cours
     */
    public void recuperationAllEnCours() throws ModelException {
        try {
            List<ChansonArtisteBean> allEnCours = dao.getAll();
            UtilFile.getStrategyFile().addEnCours(allEnCours);
        } catch (DAOException ex) {
            throw new ModelException(ex.getMessage());
        }

    }
}

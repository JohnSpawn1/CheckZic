package fr.john.spawn.hellteam.mapping;

import fr.john.spawn.hellteam.bean.ChansonBean;
import fr.john.spawn.hellteam.bean.dto.ChansonDTO;
import fr.john.spawn.hellteam.dao.AlbumDAO;
import fr.john.spawn.hellteam.error.DAOException;
import fr.john.spawn.hellteam.error.MappingException;

/**
 * Classe de mapping sur les beans 'chansons'
 */
public class MappingChanson {

    private static AlbumDAO albumDAO = new AlbumDAO();

    /**
     * Constructeur
     */
    public MappingChanson() {
        // empty
    }

    /**
     * Mapping de l'objet 'chansonDTO' en 'chansonBean'
     *
     * @param chansonDto Objet à mapper
     * @return Objet mappé
     * @throws MappingException Erreur lors du mapping
     */
    public ChansonBean mappingChansonDtoToChansonBean(ChansonDTO chansonDto) throws MappingException {
        try {
            ChansonBean chansonBean = new ChansonBean();
            chansonBean.setId(chansonDto.getId());
            chansonBean.setAlbumBean(MappingAlbum.mappingAlbumDtoToAlbumBean(albumDAO.get(chansonDto.getIdAlbum())));

            chansonBean.setLibelle(chansonDto.getLibelle());
            chansonBean.setEnCours(chansonDto.isEnCours());
            return chansonBean;
        } catch (DAOException ex) {
            throw new MappingException("chansonDto", "chansonBean");
        }
    }

}

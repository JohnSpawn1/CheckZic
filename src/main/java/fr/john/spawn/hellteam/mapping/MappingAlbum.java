package fr.john.spawn.hellteam.mapping;

import fr.john.spawn.hellteam.bean.AlbumBean;
import fr.john.spawn.hellteam.bean.dto.AlbumDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe de mapping sur l'objet Album
 */
public class MappingAlbum {

    private MappingAlbum() {
    }

    /**
     * Mapping d'une liste de string en liste d'album
     *
     * @param albumsToCreate Liste de string à convertir
     * @return Liste convertie
     */
    public static List<AlbumDTO> mappingLibellesAlbumToDto(List<String> albumsToCreate) {
        List<AlbumDTO> listAlbums = new ArrayList<>();

        albumsToCreate.forEach(album -> {
            AlbumDTO albumDTO = new AlbumDTO();
            albumDTO.setLibelle(album);
            listAlbums.add(albumDTO);
        });

        return listAlbums;

    }

    /**
     * Mapping de l'objet 'AlbumDTO' en objet 'AlbumBean'
     *
     * @param albumDTO Objet à mapper
     * @return Objet mappé
     */
    public static AlbumBean mappingAlbumDtoToAlbumBean(AlbumDTO albumDTO) {
        AlbumBean albumBean = new AlbumBean();
        albumBean.setId(albumDTO.getId());
        albumBean.setLibelle(albumDTO.getLibelle());
        return albumBean;

    }
}

package fr.john.spawn.hellteam.mapping;

import fr.john.spawn.hellteam.bean.ArtisteBean;
import fr.john.spawn.hellteam.bean.dto.ArtisteDTO;

/**
 * Classe de mapping sur les beans de 'Artiste'
 */
public class MappingArtiste {

    /**
     * Constructeur (not used)
     */
    private MappingArtiste() {
    }

    /**
     * Mapping de l'objet 'ArtistDTO' en 'ArtistBean'
     *
     * @param artisteDTO Objet à mapper
     * @return Objet mappé
     */
    public static ArtisteBean mappingArtistDtoToArtistBean(ArtisteDTO artisteDTO) {
        ArtisteBean artisteBean = new ArtisteBean();
        artisteBean.setId(artisteDTO.getId());
        artisteBean.setLibelle(artisteDTO.getLibelle());
        return artisteBean;
    }

}

# CheckZic
Récupère différentes chansons d'un fichier csv, et identifie les nouvelles

* Récupère un fichier csv avec la liste des chansons actuelles

* Ajoute les nouveaux artistes, et les affiche dans le rapport

* Ajoute les nouveaux albums, et les affiche dans le rapport

* Ajoute les nouvelles chansons, et les affiche dans le rapport

* Ajoute dans le rapport toutes les chansons en cours